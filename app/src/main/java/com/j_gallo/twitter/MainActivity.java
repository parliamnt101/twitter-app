package com.j_gallo.twitter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;


public class MainActivity extends MenuActivity {

	// Note: Your consumer key and secret should be obfuscated in your source code before shipping.
	private static final String TWITTER_KEY = "xFttKHMUqDFC0ZrwGzx2Apa0g";
	private static final String TWITTER_SECRET = "AEenOHQLLd8AxsZBM9N43aZI6HKiMDO3DqTG77CkWG7tkLXYKQ";

	private AuthUserDataSource dataSource;

	public static Activity mainActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
		Fabric.with(this, new Crashlytics(), new Twitter(authConfig));

		mainActivity = this;

        /*dataSource = new AuthUserDataSource(this);
        dataSource.open();

        AuthUser authUser = dataSource.getAuthUser();*/

		if (Twitter.getSessionManager().getActiveSession() != null) {
			Intent intent = new Intent(this, TweetListActivity.class);
			startActivity(intent);
		} else {

			setContentView(R.layout.activity_main);
			TwitterLoginButton loginButton = (TwitterLoginButton) findViewById(R.id.login_button);

			loginButton.setCallback(new Callback<TwitterSession>() {
				@Override
				public void success(Result<TwitterSession> result) {
					// Do something with result, which provides a
					// TwitterSession for making API calls
				}

				@Override
				public void failure(TwitterException exception) {
					// Do something on failure
				}
			});
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		TwitterLoginButton loginButton = (TwitterLoginButton) findViewById(R.id.login_button);

		// Pass the activity result to the login button.
		loginButton.onActivityResult(requestCode, resultCode, data);

		Intent intent = new Intent(this, TweetListActivity.class);
		startActivity(intent);
	}
}
