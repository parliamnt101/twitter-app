package com.j_gallo.twitter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

 /* Created by jgallo on 5/23/15.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_AUTH_USER = "auth_user";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TOKEN = "token";
	public static final String COLUMN_SECRET = "secret";
	public static final String COLUMN_USER_ID = "user_id";
	public static final String COLUMN_USER_NAME = "user_name";

	private static final String DATABASE_NAME = "twitter.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table "
			+ TABLE_AUTH_USER + "(" + COLUMN_ID
			+ " integer primary key autoincrement, "
			+ COLUMN_USER_ID + " varchar(255) not null default '', "
			+ COLUMN_USER_NAME + " varchar(255) not null default '', "
			+ COLUMN_SECRET + " varchar(255) not null default '', "
			+ COLUMN_TOKEN + " varchar(255) not null default '')";

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
}
