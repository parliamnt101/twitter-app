package com.j_gallo.twitter;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.twitter.sdk.android.tweetui.CompactTweetView;
import com.twitter.sdk.android.tweetui.TweetViewFetchAdapter;

/**
 * Created by jgallo on 4/5/15.
 */
public class TweetListActivity extends MenuActivity {

	final TweetViewFetchAdapter tweetAdapter =
			new TweetViewFetchAdapter<CompactTweetView>(
					TweetListActivity.this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tweet_list);

		MainActivity.mainActivity.finish();

		if (this.getWindow().getDecorView().findViewById(R.id.tweet_fragment_container_tablet) != null) {
			TweetListFragment tweetListFragment = TweetListFragment.newInstance();
			android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
			fragmentTransaction.replace(R.id.tweet_list, tweetListFragment);
			fragmentTransaction.commit();

			long tweetId = 0;
			SingleTweet singleTweetFragment = SingleTweet.newInstance(tweetId);
			android.support.v4.app.FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
			fragmentTransaction2.replace(R.id.tweet_single, singleTweetFragment);
			fragmentTransaction2.commit();
		} else {
			TweetListFragment tweetListFragment = TweetListFragment.newInstance();
			android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
			fragmentTransaction.replace(R.id.tweet_fragment_container, tweetListFragment);
			fragmentTransaction.commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}


}
