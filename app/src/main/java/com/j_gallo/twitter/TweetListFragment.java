package com.j_gallo.twitter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class TweetListFragment extends Fragment implements OnRecyclerItemClickListener {

	private RecyclerView mRecyclerView;
	private RecyclerView.Adapter mAdapter;
	private RecyclerView.LayoutManager mLayoutManager;
	private List<Tweet> tweets = new ArrayList<Tweet>();

	public static TweetListFragment newInstance() {
		TweetListFragment tweetListFragment = new TweetListFragment();
		return tweetListFragment;
	}

	public TweetListFragment() {
		super();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_tweet_list, container, false);

		mRecyclerView = (RecyclerView) view.findViewById(R.id.tweet_list_recycler);
		mRecyclerView.setHasFixedSize(true);
		mLayoutManager = new LinearLayoutManager(getActivity());
		mRecyclerView.setLayoutManager(mLayoutManager);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (mAdapter == null) {
			final TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
			StatusesService statusesService = twitterApiClient.getStatusesService();
			statusesService.homeTimeline(50, null, null, null, null, null, null,
					new Callback<List<Tweet>>() {
						@Override
						public void success(Result<List<Tweet>> result) {
							for (int i = 0; i < result.data.size(); i++) {
								Tweet tweet = result.data.get(i);
								tweets.add(tweet);
							}
							mAdapter = new CustomTweetAdapter(tweets, TweetListFragment.this);
							mRecyclerView.setAdapter(mAdapter);
						}

						public void failure(TwitterException exception) {
							Log.d("get tweets failure", exception.toString());
						}
					}
			);
		} else {
			mRecyclerView.setAdapter(mAdapter);
		}
	}

	@Override
	public void onRecyclerItemClick(Tweet tweet) {
		long tweetId = tweet.id;
		SearchResultsSingleFragment singleTweetFragment = SearchResultsSingleFragment.newInstance(tweetId);
		android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		if (getActivity().getWindow().getDecorView().findViewById(R.id.tweet_fragment_container_tablet) == null) {
			fragmentTransaction.replace(R.id.tweet_fragment_container, singleTweetFragment);
			fragmentTransaction.addToBackStack(null);
		} else {
			fragmentTransaction.replace(R.id.tweet_single, singleTweetFragment);
		}
		fragmentTransaction.commit();
	}

}
