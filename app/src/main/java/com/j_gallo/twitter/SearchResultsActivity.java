package com.j_gallo.twitter;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.twitter.sdk.android.tweetui.CompactTweetView;
import com.twitter.sdk.android.tweetui.TweetViewFetchAdapter;

/**
 * Created by jgallo on 4/5/15.
 */
public class SearchResultsActivity extends MenuActivity {

	final TweetViewFetchAdapter tweetAdapter =
			new TweetViewFetchAdapter<CompactTweetView>(
					SearchResultsActivity.this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tweet_list);
		handleIntent(getIntent());
	}

	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);

			if (this.getWindow().getDecorView().findViewById(R.id.tweet_fragment_container_tablet) != null) {
				SearchResultsListFragment searchResultsListFragment = SearchResultsListFragment.newInstance(query);
				android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
				fragmentTransaction.replace(R.id.tweet_list, searchResultsListFragment);
				fragmentTransaction.commit();

				long tweetId = 0;
				SearchResultsSingleFragment searchResultsSingleFragmentFragment = SearchResultsSingleFragment.newInstance(tweetId);
				android.support.v4.app.FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
				fragmentTransaction2.replace(R.id.tweet_single, searchResultsSingleFragmentFragment);
				fragmentTransaction2.commit();
			} else {
				SearchResultsListFragment searchResultsListFragment = SearchResultsListFragment.newInstance(query);
				android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
				fragmentTransaction.replace(R.id.tweet_fragment_container, searchResultsListFragment);
				fragmentTransaction.commit();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}


}
