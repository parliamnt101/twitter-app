package com.j_gallo.twitter;

import android.view.View;

import com.twitter.sdk.android.core.models.Tweet;

/**
 * Created by jgallo on 4/13/15.
 */
public interface OnRecyclerItemClickListener {
	public void onRecyclerItemClick(Tweet tweet);
}
