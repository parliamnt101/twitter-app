package com.j_gallo.twitter;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.LoadCallback;
import com.twitter.sdk.android.tweetui.TweetUtils;
import com.twitter.sdk.android.tweetui.TweetView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchResultsSingleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchResultsSingleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchResultsSingleFragment extends Fragment {

	public static SearchResultsSingleFragment newInstance(long tweetId) {
		SearchResultsSingleFragment tweet = new SearchResultsSingleFragment();
		Bundle args = new Bundle();
		args.putLong("tweetId", tweetId);
		tweet.setArguments(args);
		return tweet;
	}

	public SearchResultsSingleFragment() {
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_single_tweet, container, false);

		final RelativeLayout myLayout = (RelativeLayout) view.findViewById(R.id.tweet_layout);

		long tweetId = getArguments().getLong("tweetId");
		if (tweetId == 0) {
			TextView selectTweet = new TextView(getActivity());
			selectTweet.setText("Select a Tweet");
			selectTweet.setLayoutParams(new ActionBar.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.MATCH_PARENT
			));
			myLayout.addView(selectTweet);
		}
		System.out.println(tweetId);
		TweetUtils.loadTweet(tweetId, new LoadCallback<Tweet>() {
			@Override
			public void success(Tweet tweet) {
				myLayout.addView(new TweetView(getActivity(), tweet));
			}

			@Override
			public void failure(TwitterException exception) {
				//Toast.makeText(getActivity(), "sorry", Toast.LENGTH_SHORT).show();
			}
		});
		return view;
	}

}
