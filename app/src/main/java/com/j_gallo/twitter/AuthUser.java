package com.j_gallo.twitter;

/**
 * Created by jgallo on 5/23/15.
 */
public class AuthUser {
	private String token;
	private String secret;
	private long user_id;
	private String user_name;

	public AuthUser(long user_id, String user_name, String token, String secret) {
		this.user_id = user_id;
		this.user_name = user_name;
		this.secret = secret;
		this.token = token;
	}

	public long getUserId() {
		return user_id;
	}

	public void setUserId(long user_id) {
		this.user_id = user_id;
	}

	public String getUserName() {
		return user_name;
	}

	public void setUserName(String user_name) {
		this.user_name = user_name;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}
}
