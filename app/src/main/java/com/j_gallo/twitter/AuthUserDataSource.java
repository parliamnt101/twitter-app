package com.j_gallo.twitter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.twitter.sdk.android.core.TwitterSession;

import java.sql.SQLException;

/**
 * Created by jgallo on 5/23/15.
 */
public class AuthUserDataSource {
	private SQLiteDatabase db;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = {
			MySQLiteHelper.COLUMN_ID,
			MySQLiteHelper.COLUMN_USER_NAME,
			MySQLiteHelper.COLUMN_USER_ID,
			MySQLiteHelper.COLUMN_TOKEN,
			MySQLiteHelper.COLUMN_SECRET
	};

	public AuthUserDataSource(Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	public void open() {
		db = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public AuthUser setAuthUser(TwitterSession session) {
		ContentValues values = new ContentValues();

		values.put(MySQLiteHelper.COLUMN_USER_ID, session.getUserId());
		values.put(MySQLiteHelper.COLUMN_USER_NAME, session.getUserName());
		values.put(MySQLiteHelper.COLUMN_SECRET, session.getAuthToken().secret);
		values.put(MySQLiteHelper.COLUMN_TOKEN, session.getAuthToken().token);

		long insertId = db.insert(MySQLiteHelper.TABLE_AUTH_USER, null, values);

		Cursor cursor = db.query(MySQLiteHelper.TABLE_AUTH_USER, allColumns, MySQLiteHelper.COLUMN_ID
				+ " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		AuthUser authUser = cursorToAuthUser(cursor);
		cursor.close();
		return authUser;
	}

	public AuthUser getAuthUser() {
		Cursor cursor = db.query(MySQLiteHelper.TABLE_AUTH_USER, allColumns, null, null, null, null, null);

		if (cursor.getCount() == 0) {
			return null;
		}

		cursor.moveToFirst();
		AuthUser authUser = cursorToAuthUser(cursor);

		return authUser;
	}

	private AuthUser cursorToAuthUser(Cursor cursor) {
		AuthUser authUser = new AuthUser(cursor.getLong(0), cursor.getString(1),
				cursor.getString(2), cursor.getString(3));
		return authUser;
	}
}