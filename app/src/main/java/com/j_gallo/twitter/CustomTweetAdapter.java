package com.j_gallo.twitter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.*;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

/**
 * Created by jgallo on 4/8/15.
 */
public class CustomTweetAdapter extends RecyclerView.Adapter<CustomTweetAdapter.ViewHolder> {
	private static List<Tweet> mDataset;
	private OnRecyclerItemClickListener listener;
	private Context context;

	// Provide a reference to the views for each data item
	// Complex data items may need more than one view per item, and
	// you provide access to all the views for a data item in a view holder
	public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		// each data item is just a string in this case
		public TextView tweetContent;
		public TextView tweetUsername;
		public TextView tweetUserHandle;
		public ImageView tweetImage;
		private OnRecyclerItemClickListener listener;

		public ViewHolder(ViewGroup v, OnRecyclerItemClickListener listener) {
			super(v);
			this.listener = listener;
			tweetContent = (TextView) v.findViewById(R.id.tweet_content);
			tweetUsername = (TextView) v.findViewById(R.id.tweet_username);
			tweetUserHandle = (TextView) v.findViewById(R.id.tweet_userHandle);
			tweetImage = (ImageView) v.findViewById(R.id.tweet_image);
			v.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if (listener != null) {
				Tweet tweet = CustomTweetAdapter.mDataset.get(getLayoutPosition());
				listener.onRecyclerItemClick(tweet);
			}
		}
	}

	// Provide a suitable constructor (depends on the kind of dataset)
	public CustomTweetAdapter(List<Tweet> myDataset, OnRecyclerItemClickListener listener) {
		this.listener = listener;
		mDataset = myDataset;
	}

	// Create new views (invoked by the layout manager)
	@Override
	public CustomTweetAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
	                                                        int viewType) {
		// create a new view
		ViewGroup view = (ViewGroup) LayoutInflater.from(parent.getContext())
				.inflate(R.layout.list_tweet_item, parent, false);
		this.context = parent.getContext();
		ViewHolder vh = new ViewHolder(view, listener);
		return vh;
	}

	// Replace the contents of a view (invoked by the layout manager)
	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		// - get element from your dataset at this position
		// - replace the contents of the view with that element
		holder.tweetContent.setText(mDataset.get(position).text);
		holder.tweetUsername.setText(mDataset.get(position).user.name);
		holder.tweetUserHandle.setText("@" + mDataset.get(position).user.screenName);

		NetworkImageView mImageView = (NetworkImageView) holder.tweetImage;

		ImageLoader mImageLoader = MyRequestSingleton.getInstance(context).getImageLoader();

		((NetworkImageView) holder.tweetImage).setImageUrl(mDataset.get(position).user.profileImageUrl, mImageLoader);
	}

	// Return the size of your dataset (invoked by the layout manager)
	@Override
	public int getItemCount() {
		return mDataset.size();
	}
}
